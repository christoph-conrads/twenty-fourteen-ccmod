<?php
# Copyright 2015, 2016 Christoph Conrads
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );

function theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
}



add_filter( 'widget_tag_cloud_args', 'customize_tag_cloud' );

function customize_tag_cloud($args) {
	// 'largest':
	// make "linear-algebra" tag fit in one line when the primary sidebar is
	// next to the list of article, e.g., if the browser window width >=1008px
	$my_args = array(
		'smallest' => 10,
		'largest' => 18,
		'number' => 10,
		'separator' => '<br>',
		'orderby' => 'count',
		'order' => 'DESC'
	);
	$args = wp_parse_args( $args, $my_args );
	return $args;
}



# disable shortlinks
remove_action( 'wp_head', 'wp_shortlink_wp_head' );
?>
